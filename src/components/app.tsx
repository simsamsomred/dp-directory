import { h } from "preact";

import Header from "./header";

// Code-splitting is automated for `routes` directory
import Home from "../routes/home";
import { css } from "@emotion/css";

const App = () => (
  <div id="app">
    <Header />
    <Home />
    <div
      className={css`
        text-align: center;
        margin-bottom: 1rem;
      `}
    >
      Want to contribute to this?{" "}
      <a href="https://gitlab.com/simsamsomred/dp-directory/">
        Check out the code
      </a>
      . Want to contribute to the data? You can{" "}
      <a href="https://gitlab.com/simsamsomred/dp-directory/-/blob/main/src/assets/data.yml">
        edit the file
      </a>{" "}
      or{" "}
      <a href="https://gitlab.com/simsamsomred/dp-directory/-/issues">
        open an issue
      </a>
      .
    </div>
  </div>
);

export default App;
