import { Fragment, FunctionalComponent, h } from "preact";
import { css } from "@emotion/css";
import styled from "@emotion/styled";

import {
  MapContainer as LeafletMapContainer,
  TileLayer,
  Marker,
  Popup,
} from "react-leaflet";
import "leaflet/dist/leaflet.css";
import { useCallback, useEffect, useRef, useState } from "preact/hooks";
import L, { Map } from "leaflet";
import yaml from "js-yaml";

const homeCSS = css`
  padding: 78px 14px 20px;
  min-height: 100%;
  width: 100%;

  a {
    color: red;
  }
`;

const inputFilter = css`
  font-size: 1rem;
  padding: 0.25rem 0.5rem;
  width: 100%;
`;

const groupList = css`
  margin: 0;
  padding: 0;
  display: flex;
  flex-wrap: wrap;
`;

const groupListItem = css`
  display: inline-block;
  padding: 1rem;
  border: 1px solid black;
  margin: 0.5rem 0.5rem 0.5rem 0;
  flex-grow: 1;
  width: 200px;
`;

const weekdays = [
  "Sunday",
  "Monday",
  "Tuesday",
  "Wednesday",
  "Thursday",
  "Friday",
  "Saturday",
];

const colors = [
  "#f44336", // red,
  "#ff9800", // orange
  "#ffeb3b", // yellow
  "#4caf50", // green
  "#2196f3", // blue
  "#3f51b5", // indigo
  "#9c27b0", // violet
];

const dayColor = (weekday: string) => {
  return colors[weekdays.indexOf(weekday)];
};

const MapContainer = styled(LeafletMapContainer)`
  height: 340px;
  overflow: hidden;

  @media only screen and (max-height: 420px) {
    height: 200px;
  }

  & .leaflet-marker-icon {
    border-radius: 100%;
    background-clip: content-box !important;
    display: flex;
    justify-content: center;
    align-items: center;
    font-weight: bold;
    background: rgba(0, 0, 0, 0.6);
    color: white;

    ${weekdays.map(
      (w) =>
        `&.${w} {
        background: ${dayColor(w)}
      }`
    )}
  }
`;

const DC = [38.904722, -77.016389];

interface DataType {
  name: string;
  description?: string;
  type: string[];
  website: string;
  projects?: {
    name: string;
    url: string;
  }[];
  weekday: string[] | null;
  // urls?: {
  //   name: string;
  //   url: string;
  // }[];
  location?: {
    single: number[];
    name: string;
  }[];
}

type Location = { single: number[]; name: string; weekday: string[] | null };

const reduceGroups = (groups: DataType[]) => {
  return groups.reduce((aggr, g) => {
    if (g.location) {
      const mapped = g.location.map((l) => ({
        ...l,
        weekday: g.weekday,
        name: `${g.name}: ${l.name}`,
      }));
      aggr.push(...mapped);
    }
    return aggr;
  }, [] as Location[]);
};

const Home: FunctionalComponent = () => {
  const map = useRef<Map>();
  const [groups, setAllGroups] = useState<DataType[]>([]);
  const [filteredGroups, setFilteredGroups] = useState(
    groups.sort((a, b) => (a.name < b.name ? -1 : 1))
  );
  const [locations, setLocations] = useState(reduceGroups(groups));
  const [filter, setFilter] = useState("");

  useEffect(() => {
    setLocations(reduceGroups(filteredGroups));
  }, [filteredGroups]);

  useEffect(() => {
    setFilteredGroups(
      groups.filter((g) => g.name.toLowerCase().includes(filter.toLowerCase()))
    );
  }, [filter, groups]);

  const fetchData = useCallback(async () => {
    const newData = await fetch(
      "https://simsamsomred.autocode.dev/mutual-aid-map-api@dev/",
      { method: "GET" }
    );
    const { results } = await newData.json();
    console.log("results", results);

    setAllGroups(results as DataType[]);
  }, []);

  useEffect(() => {
    fetchData();
    setTimeout(() => {
      map.current?.invalidateSize();
    });
  }, [fetchData]);

  const ourMarker = (weekday?: string) => {
    console.log("weekday", weekday);
    return L.divIcon({
      className: `leaflet-marker-icon ${weekday ? weekday : ""}`,
      iconSize: L.point(20, 20, true),
    });
  };

  console.log("locations", locations);

  return (
    <div className={homeCSS}>
      <div
        className={css`
          margin: 0 auto;
        `}
      >
        <MapContainer center={DC} zoom={11} ref={map}>
          <TileLayer
            attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
            url="https://api.maptiler.com/maps/toner/{z}/{x}/{y}.png?key=CBd3mrOZy6SDUqKV2Sd2"
          />
          {locations.map((l) => (
            <Marker
              key={l.name}
              icon={ourMarker(l.weekday?.[0])}
              position={l.single}
            >
              <Popup>{l.name}</Popup>
            </Marker>
          ))}
        </MapContainer>
        <div>
          <h2>Mutual Aid Groups</h2>
          <input
            value={filter}
            className={inputFilter}
            placeholder="Filter"
            onChange={(e: any) => {
              setFilter(e.target?.value ?? "");
            }}
          />
          <ul className={groupList}>
            {filteredGroups.map((g) => (
              <li className={groupListItem} key={g.name}>
                <div>
                  <h3>{g.name}</h3>
                  <p>{g.description}</p>
                  <p>{g.website && <a href={g.website}>Website</a>}</p>
                  {g.weekday && <p>Serves on: {g.weekday?.join(", ")}</p>}
                  {g.projects && g.projects.length > 0 && (
                    <Fragment>
                      <h4>projects</h4>
                      <p>
                        {g.projects?.map((u) => (
                          <Fragment key={u.url}>
                            <a href={u.url}>{u.name}</a>{" "}
                          </Fragment>
                        ))}
                      </p>
                    </Fragment>
                  )}
                </div>
              </li>
            ))}
          </ul>
        </div>
      </div>
    </div>
  );
};

export default Home;
